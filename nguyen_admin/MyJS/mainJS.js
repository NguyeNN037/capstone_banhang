import SanPhamService from "./controller.js";
import SanPham from "./model.js";
let dssSP = new SanPhamService();

// Bộ hàm để render và lấy dữ liệu
function renderDanhSachSP(danhSach) {
  let content = "";
  let count = 1;
  danhSach.map(function (sp) {
    var productMoneyFormat = new Intl.NumberFormat("de-DE").format(sp.price);
    content += `
    <tr>
    <td>${count}</td>
    <td>${sp.name}</td>
    <td>${productMoneyFormat}</td>
    <td>${sp.screen}</td>
    <td>${sp.backCamera}</td>
    <td>${sp.frontCamera}</td>
    <td>
      <div class="item__image" >
        <a href="#">
          <img  style="max-width:60%; height:60%; display:block;" src="${sp.img}" alt="">
        </a>
      </div>
    </td>
    <td>${sp.desc}</td>
    <td>${sp.type}</td>
    <td>
        <button
        onclick="window.xemSP(${sp.id})" class="btn btn-success"
        >Xem</button>

       <button onclick="xoaSP(${sp.id})" class="btn btn-danger">Xóa</button>

    </td>
    </tr>`;
    count++;
  });
  document.getElementById("tblDanhSachSP").innerHTML = content;
  return danhSach;
}
// phần render lại nút thêm sản phẩm
function renderAddButton() {
  resetForm();
  document.querySelector(
    ".modal-footer"
  ).innerHTML = `<button onclick="themSP()" class="btn btn-success">Thêm sản phẩm</button>`;
}

document.getElementById("btnThemSP").addEventListener("click", renderAddButton);

function setLocalStorage(danhSach) {
  localStorage.setItem("DSSP", JSON.stringify(danhSach));
}
function getLocalStorage() {
  var mangKQ = JSON.parse(localStorage.getItem("dssSP"));
  return mangKQ;
}

function getListProducts() {
  dssSP
    .layDanhSach()
    .then(function (result) {
      console.log(result.data);
      renderDanhSachSP(result.data);
      setLocalStorage(result.data);
    })
    .catch(function (error) {
      console.log("error: ", error);
    });
  return dssSP;
}

getListProducts();

// Tìm kiếm
function searchSP() {
  var mangTK = [];
  var chuoiTK = document.getElementById("inputSP").value;
  mangTK = dssSP.timKiemSanPham(dssSP, chuoiTK);
  console.log("mangTK: ", mangTK);
  renderDanhSachSP(mangTK);
}
window.searchSP = searchSP;
// Hàm chức năng:
function themSP() {
  var tenSP = document.getElementById("tenSP").value;
  var giaSP = document.getElementById("giaSP").value;
  var moTaSP = document.getElementById("moTaSP").value;
  var anhSP = document.getElementById("anhSP").value;
  var cameraTruoc = document.getElementById("cameraFSP").value;
  var cameraSau = document.getElementById("cameraBSP").value;
  var manHinh = document.getElementById("screenSP").value;
  var loai = document.getElementById("loaiSP").value;
  var newSP = new SanPham(
    tenSP,
    giaSP,
    manHinh,
    cameraTruoc,
    cameraSau,
    anhSP,
    moTaSP,
    loai
  );
  dssSP
    .themSanPham(newSP)
    .then(function (result) {
      console.log(result.data);
      getListProducts();
    })
    .catch(function (error) {
      console.log("error", error);
    });
}
window.themSP = themSP;

function xoaSP(maSP) {
  dssSP
    .xoaSanPham(maSP)
    .then(function (result) {
      getListProducts();
    })
    .catch(function (error) {
      console.log("error", error);
    });
}
window.xoaSP = xoaSP;

function xemSP(maSP) {
  dssSP
    .xemSanPham(maSP)
    .then(function (res) {
      console.log("res: ", res);
      $("#myModal").modal("show");
      document.getElementById("tenSP").value = res.data.name;
      document.getElementById("giaSP").value = res.data.price;
      document.getElementById("moTaSP").value = res.data.desc;
      document.getElementById("anhSP").value = res.data.img;
      document.getElementById("cameraFSP").value = res.data.frontCamera;
      document.getElementById("cameraBSP").value = res.data.backCamera;
      document.getElementById("screenSP").value = res.data.screen;
      document.getElementById("loaiSP").value = res.data.type;

      var footerEL = (document.querySelector(
        ".modal-footer"
      ).innerHTML = `<button onclick="themMoiSP(${res.data.id})" class="btn btn-success">Cập Nhập lại</button>`);
    })
    .catch(function (error) {
      console.log("error", error);
    });
}
window.xemSP = xemSP;

function themMoiSP(maSP) {
  var tenSP = document.getElementById("tenSP").value;
  var giaSP = document.getElementById("giaSP").value;
  var moTaSP = document.getElementById("moTaSP").value;
  var anhSP = document.getElementById("anhSP").value;
  var cameraTruoc = document.getElementById("cameraFSP").value;
  var cameraSau = document.getElementById("cameraBSP").value;
  var manHinh = document.getElementById("screenSP").value;
  var loai = document.getElementById("loaiSP").value;
  var newSP = new SanPham(
    tenSP,
    giaSP,
    manHinh,
    cameraSau,
    cameraTruoc,
    anhSP,
    moTaSP,
    loai
  );

  dssSP
    .capNhapSanPham(maSP, newSP)
    .then(function (res) {
      getListProducts();
      document.querySelector("#myModal .close").click();
    })
    .catch(function (error) {
      console.log("maSP: ", maSP);
      console.log("error: ", error);
    });
}
window.themMoiSP = themMoiSP;

function resetForm() {
  document.getElementById("modalSP").reset();
}
