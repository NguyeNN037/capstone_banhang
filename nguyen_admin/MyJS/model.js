export default class SanPham {
  constructor(
    _name,
    _price,
    _screen,
    _backCamera,
    _frontCamera,
    _img,
    _desc,
    _type
  ) {
    this.name = _name;
    this.price = _price;
    this.screen = _screen;
    this.backCamera = _backCamera;
    this.frontCamera = _frontCamera;
    this.img = _img;
    this.desc = _desc;
    this.type = _type;
  }
}

// export default function SanPham(_id, _name, _price) {
//   this.tenSP = _ten;
//   this.giaSP = _gia;
//   this.anhSP = _hinhanh;
//   this.motaSP = _mieuta;
//   return;
// }
