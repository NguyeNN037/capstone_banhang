export default function SanPhamService() {
  const BASE_URL = "https://633ec05f0dbc3309f3bc54ea.mockapi.io";

  this.layDanhSach = function () {
    var dssSP = axios({
      url: `${BASE_URL}/sp`,
      method: "GET",
    });

    return dssSP;
  };

  this.themSanPham = function (sanPham) {
    var dssSP = axios({
      method: "POST",
      url: `${BASE_URL}/sp`,
      data: sanPham,
    });
    return dssSP;
  };

  this.xoaSanPham = function (maSP) {
    var dssSP = axios({
      url: `${BASE_URL}/sp/${maSP}`,
      method: "DELETE",
    });
    return dssSP;
  };

  this.xemSanPham = function (id) {
    var sanPham = axios({
      url: `${BASE_URL}/sp/${id}`,
      method: "GET",
    });
    return sanPham;
  };

  this.capNhapSanPham = function (maSP, sanPham) {
    var dssSP = axios({
      method: "put",
      url: `${BASE_URL}/sp/${maSP}`,
      data: sanPham,
    });
    return dssSP;
  };

  this.timKiemSanPham = function (danhSach, sanPham) {
    var mangTK = [];
    mangTK = danhSach.map(function (sp) {
      return (sp.tenSP = sanPham);
    });
  };
}
