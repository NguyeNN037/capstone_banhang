// function render product list;
var renderDanhSachSanPham = function (listSp) {
  var contentHTML = "";
  listSp.forEach(function (sp) {
    contentHTML += ` <div class="product__item col-6 col-sm-6 col-md-6 col-lg-4 col-xl-3">
                        <div class="item__image">
                            <a href="#"><img src="${sp.img}" alt=""></a>
                        </div>

                        <div class="item__detail">
                            <div class="item__main">
                                <a href="#">
                                    <h1 style="font-weight:bold;font-size: 18px;">${
                                      sp.name
                                    }</h1>
                                </a>
                                <p><span>${new Intl.NumberFormat(
                                  "de-DE"
                                ).format(
                                  sp.price
                                )}</span><sup style="color:red;font-weight:bold">đ</sup></p>
                            </div>
                            <div class="item__description">
                                <p>${sp.screen}<br>
                                    ${sp.backCamera}<br>
                                    ${sp.frontCamera}<br>
                                    ${sp.desc}<br>
                                </p>
                            </div>
                            <button onclick="addCart('${
                              sp.id
                            }')">thêm vào giỏ hàng</button>
                        </div>
                      </div>             
    `;
  });
  document.getElementById("product__items").innerHTML = contentHTML;
};

// function render Cart
let renderCart = (listCart) => {
  var contentHTML = "";
  var sumMoney = 0;
  for (var i = 0; i < listCart.length; i++) {
    var currentCart = listCart[i].product;
    var productMoney = currentCart.price * listCart[i].quantity;
    var productMoneyFormat = new Intl.NumberFormat("de-DE").format(
      productMoney
    );
    var contentTr = `<tr>
                        <td class="product"><img src="${currentCart.img}" alt=""></td>
                        <td>${currentCart.name}</td>
                        <td>
                            <p><span>${productMoneyFormat}</span><sup>đ</sup></p>
                        </td>
                        <td>
                        <input onclick=subQuantity(${currentCart.id}) type="button" value="-" style="padding: 0 10px">
                        <input type="text" value="${listCart[i].quantity}" style="width:40px" id="text-box">
                        <input onclick=addQuantity(${currentCart.id}) type="button" value="+" style="padding: 0 10px">
                        </td>
                        <td>
                        <button onclick="deleteProductCart(${currentCart.id})" class="btn btn-success">Xóa</button>
                        </td>
                    </tr>
                    `;
    contentHTML += contentTr;
    sumMoney += productMoney;
  }

  document.getElementById("cart__total").innerHTML = contentHTML;
  document.getElementById("sumMoney").innerHTML = `${new Intl.NumberFormat(
    "de-DE"
  ).format(sumMoney)} VND`;
};

// function render Cart
let renderOrder = (listCart) => {
  var contentHTML = "";
  var moneyTotal = 0;
  for (var i = 0; i < listCart.length; i++) {
    var currentCart = listCart[i].product;
    var productMoney = currentCart.price * listCart[i].quantity;
    var productMoneyFormat = new Intl.NumberFormat("de-DE").format(
      productMoney
    );
    var contentTr = `<tr>
                         <td>${listCart[i].quantity}</td>
                         <td>${currentCart.name}</td>
                         <td class="product"><img src="${currentCart.img}" alt="" style="width: 80px;height:80px"></td>
                         <td>
                            <p><span>${productMoneyFormat}</span><sup>đ</sup></p>
                        </td>
                    </tr>
                    `;
    contentHTML += contentTr;
    moneyTotal += productMoney;
  }
  document.getElementById("order__list").innerHTML = contentHTML;
  document.getElementById("moneyTotal").innerHTML = `${new Intl.NumberFormat(
    "de-DE"
  ).format(moneyTotal)} VND`;
};

// save localStorage products
let saveLocalStorageProduct = () => {
  var productListJson = JSON.stringify(productList);
  localStorage.setItem("PRODUCT", productListJson);
};
// save localStorage cart
let saveLocalStorageCart = () => {
  var cartListJson = JSON.stringify(cart);
  localStorage.setItem("CART", cartListJson);
};

// click ẩn hiên giỏ hàng ra màn hình.
let clickShowCart = () => {
  let cartBtn = document.querySelector(".fa-times");
  let cartShow = document.querySelector(".cart-icon");

  cartShow.addEventListener("click", function () {
    document.querySelector(".my__cart").style.right = "0";
    document.querySelector(".my__cart").style.left = "0";
    document.querySelector(".my__cart").style.backgroundColor =
      "rgba(0, 0, 0, 0.5)";
    document.querySelector(".my__cart").style.transition = "all 1s";
  });
  cartBtn.addEventListener("click", function () {
    document.querySelector(".my__cart").style.right = "-100%";
    document.querySelector(".my__cart").style.left = "";
  });
};
clickShowCart();

// click ẩn hiên Order ra màn hình.
let clickShowOrder = () => {
  let OrderBtn = document.querySelector(".latch__Order");
  let comeBackBtn = document.querySelector(".come__back");

  OrderBtn.addEventListener("click", function () {
    document.querySelector(".order__show").style.left = "50%";
    document.querySelector(".cart").style.right = "-100%";
  });
  comeBackBtn.addEventListener("click", function () {
    document.querySelector(".order__show").style.left = "-100%";
    document.querySelector(".cart").style.right = "0";
  });
};
clickShowOrder();

// click ẩn hiên đặt hàng thành công ra màn hình.
let clickSuccess = () => {
  let datHang = document.querySelector(".dat_hang");
  datHang.addEventListener("click", function () {
    document.querySelector(".success__order").style.top = "50%";
    document.querySelector(".order__show").style.left = "-100%";
    clearCart();
  });
};
clickSuccess();

// click ẩn hiên continue ra màn hình.
let clickContinue = () => {
  let okBtn = document.querySelector(".success__ok");
  okBtn.addEventListener("click", function () {
    document.querySelector(".continue__Purchase").style.top = "50%";
    document.querySelector(".success__order").style.top = "-100%";
  });
};
clickContinue();

// click function continue
let newCart = () => {
  let newCart = document.querySelector(".new__cart");
  newCart.addEventListener("click", function () {
    location.reload();
  });
};
newCart();
