// tạo mảng sản phẩm
var productList = [];
// lấy productList dưới local lên và chuyển đổi thành array rồi cập nhật lại dạo diện
var dataProductJson = localStorage.getItem("PRODUCT");
if (dataProductJson) {
  productList = JSON.parse(dataProductJson);
  renderDanhSachSanPham(productList);
}

// tạo mảng giỏ hàng.
var cart = [];
// lấy cart dưới local lên và chuyển đổi thành array rồi cập nhật lại dạo diện
var dataCartJson = localStorage.getItem("CART");
if (dataCartJson) {
  cart = JSON.parse(dataCartJson);
  renderCart(cart);
  renderOrder(cart);
}

// lấy số lương dưới local lên và chuyển đổi thành số rồi cập nhật lại dạo diện
var totalQuantityCart = 0;
var dataQuantityCartJson = localStorage.getItem("QUANTITY-CART");
if (dataQuantityCartJson) {
  totalQuantityCart = JSON.parse(dataQuantityCartJson);
  document.getElementById("total-cart").innerHTML = totalQuantityCart;
}

// get API
const BASE_URL = "https://633ec05f0dbc3309f3bc54ea.mockapi.io";
axios({
  url: `${BASE_URL}/sp`,
  method: "GET",
})
  .then(function (response) {
    productList = response.data;
    // Lưu product xuống localStorage.
    saveLocalStorageProduct();
  })
  .catch(function (error) {
    console.log("error: ", error);
  });

// function lọc sản phẩm theo loại
function chonDT(obj) {
  var value = obj.value;
  if (value == "samSung") {
    var chonDT = [];
    for (let i = 0; i < productList.length; i++) {
      if (productList[i].type == "Samsung") {
        chonDT.push(productList[i]);
      }
    }
  } else if (value == "iphone") {
    var chonDT = [];
    for (let i = 0; i < productList.length; i++) {
      if (productList[i].type == "Iphone") {
        chonDT.push(productList[i]);
      }
    }
  } else if (value == "oppo") {
    var chonDT = [];
    for (let i = 0; i < productList.length; i++) {
      if (productList[i].type == "Oppo") {
        chonDT.push(productList[i]);
      }
    }
  } else if (value == "" || value == "all") {
    renderDanhSachSanPham(productList);
  }
  renderDanhSachSanPham(chonDT);
}
// function add cart
function addCart(idSp) {
  var indexProduct;
  for (let i = 0; i < productList.length; i++) {
    if (productList[i].id === idSp) {
      indexProduct = i;
    }
  }

  var check = false;
  var index;
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].product.id === idSp) {
      check = true;
      index = i;
    }
  }

  if (check) {
    var result = confirm(
      "Sản phẩm của bạn đã tồn tại, bạn có muốn thêm không?"
    );
    if (result == true) {
      cart[index].quantity = cart[index].quantity + 1;
    } else {
      return;
    }
  } else {
    let cartItem = { product: productList[indexProduct], quantity: 1 };
    cart.push(cartItem);
  }
  // Lưu  cart xuống localStorage.
  saveLocalStorageCart();
  totalCart();
  renderCart(cart);
  renderOrder(cart);
}

// function delete product cart
let deleteProductCart = (idSp) => {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].product.id == idSp) {
      cart.splice(i, 1);
    }
  }
  // save cart xuống localStorage.
  saveLocalStorageCart();
  totalCart();
  renderCart(cart);
  renderOrder(cart);
};

// function add quantity cart
function addQuantity(idSp) {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].product.id == idSp) {
      cart[i].quantity = cart[i].quantity + 1;
    }
  }
  // save cart xuống localStorage.
  saveLocalStorageCart();
  totalCart();
  renderCart(cart);
  renderOrder(cart);
}

// function sub quantity cart
function subQuantity(idSp) {
  for (let i = 0; i < cart.length; i++) {
    if (cart[i].product.id == idSp) {
      cart[i].quantity = cart[i].quantity - 1;
    }
    if (cart[i].quantity <= 0) {
      cart.splice(i, 1);
      // Lưu cart xuống localStorage.
      saveLocalStorageCart();
      renderCart(cart);
      renderOrder(cart);
      totalCart();
      return;
    }
  }
  // save cart xuống localStorage.
  saveLocalStorageCart();
  totalCart();
  renderCart(cart);
  renderOrder(cart);
}

// function reset cart
let clearCart = () => {
  if (cart != null) {
    //  cart.splice(0);
    cart.length = 0;
  }
  // save cart xuống localStorage.
  saveLocalStorageCart();
  totalCart();
  renderCart(cart);
  renderOrder(cart);
};

// function total quantity
let totalCart = () => {
  let totalCart = 0;
  for (let index = 0; index < cart.length; index++) {
    totalCart += cart[index].quantity;
  }

  document.getElementById("total-cart").innerHTML = totalCart;
  // Lưu total quantity Cart xuống localStorage.
  var cartQuantityJson = JSON.stringify(totalCart);
  localStorage.setItem("QUANTITY-CART", cartQuantityJson);

  return totalCart;
};

// function search product
function searchProduct() {
  // lấy giá trị từ ô tìm kiếm
  var nameValue = document.getElementById("txtSearch").value;
  var productSearch = productList.filter((value) => {
    return value.name.toUpperCase().includes(nameValue.toUpperCase());
  });
  renderDanhSachSanPham(productSearch);
}
